var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({
    pattern: ['gulp-*']
});
var merge = require('merge-stream');
var mainBowerFiles = require('gulp-main-bower-files');
var gulpFilter = require('gulp-filter');



gulp.task('jsConcat', function(){
    return gulp.src('./public_old/js/**/*.js')
        .pipe(plugins.debug({title: 'jsConcat:'}))
        .pipe(plugins.concat('scripts.js'))
        .pipe(plugins.minifier({
            minify: true,
            collapseWhitespace: true,
            conservativeCollapse: true,
            minifyJS: true
        }))
        .pipe(plugins.rename('scripts.min.js'))
        .pipe(gulp.dest('./public/'))

});


gulp.task('cssConcat', function() {
    var pathsToMinify = [
        './public_old/css/style.css'
    ];
    var pathsToConcat = [
        './public_old/css/bootstrap/*.css'
    ];
    var minStream = gulp.src(pathsToMinify)
        .pipe(plugins.minifier({
            minify: true,
            collapseWhitespace: true,
            conservativeCollapse: true,
            minifyCSS: true
        }));
    var concatStream = gulp.src(pathsToConcat);
    return merge(minStream, concatStream)
        .pipe(plugins.concat('styles.min.css'))
        .pipe(gulp.dest('./public/'));

});


gulp.task('default',['jsConcat', 'cssConcat'] ,function () {
    var target = gulp.src('./public/index.html');
    var sources = gulp.src(['./public/*.js', './public/*.css'], {read: false});

    return target.pipe(plugins.inject(sources, {relative: true}))
        .pipe(gulp.dest('./public'));
});


gulp.task('main-bower-files', function() {
    var filterJS = gulpFilter('**/*.js', { restore: true });
    return gulp.src('./public/bower.json')
        .pipe(mainBowerFiles({
            overrides: {
                bootstrap: {
                    main: [
                        './dist/js/bootstrap.js',
                        './dist/css/*.min.*',
                        './dist/fonts/*.*'
                    ]
                }
            }
        }))
        .pipe(plugins.debug({title: 'main-bower-files:'}))
        .pipe(filterJS)
        .pipe(plugins.concat('vendor.js'))
        .pipe(filterJS.restore)
        .pipe(gulp.dest('./public/vendor'));
});


// pug file
gulp.task('views', function buildHTML() {
    return gulp.src('*.pug')
        .pipe(plugins.pug())
        .pipe(gulp.dest('test'));
});






